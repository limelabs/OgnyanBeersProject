import { BeerDetailsComponent } from './beer-detail/beer.details.component';
import { DataService } from './data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BeersComponent } from './beers/beers.component';
import { HttpModule } from '@angular/http';
import { TextLimitPipe } from './text-limit.pipe';
import { FavoriteComponent } from './favorite/favorite.component';
import { FavListComponent } from './fav-list/fav-list.component';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { RouterLinkActive } from '@angular/router';
import { SearchFormComponent } from './search-form/search-form.component';
import { PaginationComponent } from './pagination/pagination.component';
import { MessagesComponent } from './messages/messages.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BeersComponent,
    TextLimitPipe,
    FavoriteComponent,
    FavListComponent,
    SearchComponent,
    BeerDetailsComponent,
    SearchFormComponent,
    PaginationComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      
      {path: 'favorites', component: FavListComponent},
      {path: 'beer/:id', component: BeerDetailsComponent},
      {path: 'search', component: SearchComponent},
      {path: '', component: BeersComponent},
     
    ])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

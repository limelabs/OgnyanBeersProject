import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  pageNumber = 2;
  constructor() { }
  
  ngOnInit() {
  }

  nextPage() {
    this.pageNumber++;
    console.log('increment', this.pageNumber);
  }

  prevPage() {
    this.pageNumber--;
    console.log('decrement', this.pageNumber);
  }
}

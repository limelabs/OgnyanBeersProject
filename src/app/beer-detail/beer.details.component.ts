import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { DataService } from './../data.service';

@Component({
  selector: 'beer-details',
  templateUrl: './beer.details.component.html',
  styleUrls: ['./beer.details.component.scss']
})

export class BeerDetailsComponent implements OnInit {
  beer: {};
  id: number;

  constructor (private data: DataService, private route: ActivatedRoute) {
    this.id = Number(this.route.snapshot.paramMap.get('id'))
  }

  ngOnInit () {
    this.data.getSingleBeer(this.id)
    .subscribe( responce => {
      this.beer = responce.json()[0];
    });
  }

}
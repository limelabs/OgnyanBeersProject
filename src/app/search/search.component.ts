import { ActivatedRoute } from '@angular/router';
import { FavoriteChangedEventArgs } from './../favorite/favorite.component';
import { DataService } from './../data.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  // searchString: string;
  @Output() change = new EventEmitter();
  searchBeers = [];
  query: string;
  isAdded: boolean;
  isVisible: boolean;
  message: string;
  constructor(private route: ActivatedRoute, private data: DataService, private favoriteSerice: DataService) {}

  ngOnInit() {
   this.route.queryParamMap.subscribe();
   this.query = this.route.snapshot.queryParamMap.get('query');
   this.searchData(this.query);
  }
  searchData(query) {
    this.data.search(query)
    .subscribe(responce => {
      this.searchBeers = responce.json();
    });
  }

  onFavoriteChange(eventArgs: FavoriteChangedEventArgs, ) {
    this.favoriteSerice.addFavorites(eventArgs);

    if (eventArgs.isFavoriteNow) {
      this.isAdded = true;
      this.isVisible = true;
      this.timeout();
      if (this.isAdded) {
        this.message = 'Added to Favorite!';
      } else {
        this.message = 'Removed from Favorite!';
      }
    } else {
      this.isAdded = false;
      this.isVisible = true;
      this.timeout();
      if (this.isAdded) {
        this.message = 'Added to Favorite!';
      } else {
        this.message = 'Removed from Favorite!';
      }
    }
  }
  isFavorite(id) {
    return this.favoriteSerice.has(id);
   }

   timeout() {
    setTimeout(() => {
    this.isVisible = !this.isVisible;
    }, 1500);
  }
}

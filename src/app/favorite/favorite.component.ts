import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})
export class FavoriteComponent implements OnInit {
  @Input('isFavorite') isSelected = false;
  @Output('change') click = new EventEmitter();
  @Input('beer') beer;

  favorite() {
    this.isSelected = !this.isSelected;
    this.click.emit({ isFavoriteNow: this.isSelected , beer: this.beer});
  }
  constructor() { }

  ngOnInit() {
  }

}
export interface FavoriteChangedEventArgs {
  isFavoriteNow: boolean;
  beer:any;
}

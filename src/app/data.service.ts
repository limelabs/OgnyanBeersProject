import { FavoriteChangedEventArgs } from './favorite/favorite.component';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';



@Injectable()
export class DataService {
  favorites = [];
  private url = 'https://api.punkapi.com/v2/beers';
  constructor(private http: Http) { }

  getBeers() {
    return this.http.get(this.url);
  }

  getBeerPage(pageNumber) {
    return this.http.get(this.url + `?page=${pageNumber}`);
  }

  getBeersByIds(ids: any[]) {
    return this.http.get(this.url + '?ids=' + ids.join('|'));
  }

  addFavorites(eventArgs: FavoriteChangedEventArgs) {
    if (eventArgs.isFavoriteNow) {
      this.favorites.push(eventArgs.beer.id);
    } else {
     this.delete(eventArgs.beer.id);
    }
  }

  has(id: number) {
    return this.favorites.includes(id);
  }


  getAllFavorites() {
    return this.favorites;
  }

  search(querryString) {
    let queryURL = `${this.url}/?beer_name=${querryString}`;
    return this.http.get(queryURL);
  }

  getSingleBeer(id) {
    let queryURL = `${this.url}/${id}`;
    return this.http.get(queryURL);
  }

  delete(id: number) {
    let index = this.favorites.indexOf(id);
    this.favorites.splice(index, 1);
  }
}

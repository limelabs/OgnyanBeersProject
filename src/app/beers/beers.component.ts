import { ActivatedRoute } from '@angular/router';
import { FavoriteChangedEventArgs } from './../favorite/favorite.component';
import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit {
  beers = [];
  beer;
  isSearching: boolean;
  pageNumber;
  isAdded: boolean;
  isVisible: boolean;
  message: string;
  constructor(private beerData: DataService, private favoriteSerice: DataService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.queryParamMap
      .subscribe( params => {
          this.pageNumber = params.get('page');
          if (this.pageNumber)  {
            this.beerData.getBeerPage(this.pageNumber)
            .subscribe( responce => {
              this.beers = responce.json();
            });
          }
        }
      );
        this.beerData.getBeers()
        .subscribe( responce => {
          this.beers = responce.json();
        });
  }

  onFavoriteChange(eventArgs: FavoriteChangedEventArgs, ) {
    this.favoriteSerice.addFavorites(eventArgs);
    if (eventArgs.isFavoriteNow) {
      this.isAdded = true;
      this.isVisible = true;
      this.timeout();
      if (this.isAdded) {
        this.message = 'Added to Favorite!';
      } else {
        this.message = 'Removed from Favorite!';
      }
    } else {
      this.isAdded = false;
      this.isVisible = true;
      this.timeout();
      if (this.isAdded) {
        this.message = 'Added to Favorite!';
      } else {
        this.message = 'Removed from Favorite!';
      }
    }
  }
  isFavorite(id) {
   return this.favoriteSerice.has(id);
  }

  timeout() {
    setTimeout(() => {
    this.isVisible = !this.isVisible;
    }, 1500);
  }
}

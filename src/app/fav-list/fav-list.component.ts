import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fav-list',
  templateUrl: './fav-list.component.html',
  styleUrls: ['./fav-list.component.scss']
})
export class FavListComponent implements OnInit {
  favoriteBeers: any[];
  constructor(private favoriteService: DataService) { }

  ngOnInit() {
    this.favoriteService.getBeersByIds(this.favoriteService.getAllFavorites())
    .subscribe( responce => {
      this.favoriteBeers = responce.json();
    });
  }

}
